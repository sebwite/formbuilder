# README Sebwite FormBuilder #

## Installatie ##
Voeg de package toe aan Composer.json


```
#!php

{
     "type": "vcs",
      "url": "https://SilvanLaroo@bitbucket.org/sebwite/formbuilder.git"
}
```

Require package in composer.json


```
#!php
"sebwite/form-builder" : "dev-master"

```

Laat composer de package installeren door composer update te draaien.

Voeg de ServiceProvider toe aan app.php:

```
#!php
'Sebwite\FormBuilder\FormBuilderServiceProvider'
```

## Configuratie Aanpassen ##
```
#!php
php artisan config:publish sebwite/form-builder
```