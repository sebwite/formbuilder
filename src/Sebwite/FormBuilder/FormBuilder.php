<?php namespace Sebwite\FormBuilder;
/*
|--------------------------------------------------------------------------
|   FormBuilder
|--------------------------------------------------------------------------
|
|   Extension of the formbuilder class
|
|   @author Silvan
|   @date 01 april 2014
|   @time 08:37
*/

use Illuminate\Html\FormBuilder as IlluminateFormBuilder;
use Psr\Log\InvalidArgumentException;
use \Input;
use \Config;

/**
 * Class FormBuilder
 * @package Sebwite\FormBuilder
 */
class FormBuilder extends IlluminateFormBuilder {

    /**
     * @var string
     */
    protected $defaultWrapperClass = 'form-group form-center';


    /**
     * @param $name
     * @param $placeholder
     * @param $errors
     * @param array $options
     * @param null $value
     * @return string
     */
    public function textField($name, $placeholder, $errors, $options = [], $value = null)
    {
        return $this->field('text', $name, $placeholder, $errors, $options, $value);
    }

    /**
     * @param $name
     * @param $placeholder
     * @param $errors
     * @param array $options
     * @param null $value
     * @return string
     */
    public function passwordField($name, $placeholder, $errors, $options = [], $value = null)
    {
        return $this->field('password', $name, $placeholder, $errors, $options, $value);
    }

    /**
     * @param $name
     * @param $placeholder
     * @param $errors
     * @param array $options
     * @param null $value
     * @return string
     */
    public function emailField($name, $placeholder, $errors, $options = [], $value = null) {
        return $this->field('email', $name, $placeholder, $errors, $options, $value);
    }

    /**
     * @param $name
     * @param $placeholder
     * @param $errors
     * @param array $options
     * @param null $value
     * @return string
     */
    public function timeField($name, $placeholder, $errors, $options = [], $value = null)
    {
        $defaultOptions = [
            'class' => 'text-center'
        ];

        $options = array_merge($defaultOptions, $options);

        return $this->field('time', $name, $placeholder, $errors, $options, $value);
    }

    /**
     * @param $name
     * @param $placeholder
     * @param $errors
     * @param array $options
     * @param null $value
     * @return string
     */
    public function zipcodeField($name, $placeholder, $errors, $options = [], $value = null) 
    {
    	$options['class'] = trim((isset($options['class']) ? $options['class'] : '') . ' input-zipcode');

        return $this->field('text', $name, $placeholder, $errors, $options, $value);
    }

    /**
     * @param $value
     * @param array $options
     * @return string
     */
    public function submitButton($value, $options = [])
    {
        if(!isset($options['wrapperClass']))
            $options['wrapperClass'] = 'form-group form-right';

        return $this->wrap($this->submit($value, array_except($options, ['wrapperClass'])), $options);
    }

    /**
     * @param $name
     * @param $begin
     * @param $end
     * @param array $options
     * @param $errors
     * @return string
     */
    public function selectMonthRange($name, $begin, $end, $options = [], $errors = []) {
        $loop = $end - $begin;

        if( ! isset($options['textual'])) {
            $selectArray = [0 => 'Geen'];
        } else {
            $selectArray = [0 => 'Alle maanden'];
        }

        for($i = 0; $i <= $loop; $i++) {

            if(isset($options['textual'])) {
                $selectArray[$begin] = strftime('%B', strtotime(date('Y-m-d', strtotime(date('Y'). '-'.$begin.'-01'))));
            } else {
                $selectArray[$begin] = ($begin > 1) ? $begin.' maanden' : $begin.' maand';
            }

            $begin++;
        }

        return $this->selectField($name, $selectArray, $this->getValueAttribute($name) , $options, $errors);
    }

    /**
     * @param $name
     * @param $placeholder
     * @param $errors
     * @param array $options
     * @param null $value
     * @return string
     */
    public function textareaField($name, $placeholder, $errors, $options = [], $value = null)
    {
        $html = '';

        $options['placeholder'] = $placeholder;

        $default = [
          'cols'    => '',
          'class'   => 'autosize',
        ];

        $options = array_merge($default, $options);

        // Loop through errors, when error found for textarea with given name, add class form-error
        if($errors and $errors->has($name)) {
            $options['wrapperClass'] = isset($options['wrapperClass']) ? $options['wrapperClass'] . ' form-error ' : 'form-error ';
            $options['class'] = isset($options['class']) ? $options['class'] . ' form-error ' : 'form-error ';
        }

        // Create label
        $html .= $this->label($name, $placeholder);

        // Check if label is set. If so - just show normal label by not adding the class has-label-placeholder
        if(!isset($options['label']) or $options['label'] == false) {
            $options['wrapperClass'] = isset($options['wrapperClass']) ? $options['wrapperClass'] . ' has-label-placeholder' : $this->defaultWrapperClass. ' has-label-placeholder ';
        }

        $html .= $this->textarea($name, $value, array_except($options, ['label', 'before', 'after', 'wrapperClass', 'tooltip', 'placeholder']));

        return $this->wrap($html, $options);
    }


    /**
     * @param $radioArray
     * @param $checked
     * @param $errors
     * @param array $options
     * @return string
     */
    public function radioGroupField($radioArray, $checked, $errors, $options = [])
    {
        return $this->multiGroup('radios', $radioArray, $checked, $errors, $options);
    }

    /**
     * @param $item
     * @param $name
     * @param $checked
     * @return string
     */
    private function radioInput($item, $name, $checked){
        return $this->multiInput('radio', $item, $name, $checked);
    }

    /**
     * @param $checkboxArray
     * @param $checked
     * @param $errors
     * @param array $options
     * @return string
     */
    public function checkboxGroupField($checkboxArray, $checked, $errors, $options = [])
    {
        return $this->multiGroup('checkboxes', $checkboxArray, $checked, $errors, $options);
    }

    /**
     * @param $item
     * @param $name
     * @param $checked
     * @return string
     */
    private function checkboxInput($item, $name, $checked){
        return $this->multiInput('checkbox', $item, $name, $checked);
    }

    /**
     * Container for checkbox / radio input item
     *
     * @param $type
     * @param $item
     * @param $name
     * @param $checked
     * @return string
     * @throws \Psr\Log\InvalidArgumentException
     */
    private function multiInput($type, $item, $name, $checked)
    {
        $html = '';

        if(!isset($item['value']))
            throw new InvalidArgumentException($type.' value attribute not defined.');

        if(!isset($item['placeholder']))
            throw new InvalidArgumentException($type.' placeholder attribute not defined.');

        // Check if the current item is checked
        if($checked === $item['value'] || $checked == 'checked' || $checked == true) {
            $item['checked'] = true;
        } else
            $item['checked'] = false;

        // Set the id of the input based on the value
        $item['options']['id'] = isset($item['options']['id']) ? $item['options']['id'] : $item['value'];

        // Add html for the input based on the type of the input
        if($type == 'checkbox')
            $html .= $this->checkbox($name, $item['value'], $item['checked'], $item['options']);
        elseif($type == 'radio')
            $html .= $this->radio($name, $item['value'], $item['checked'], $item['options']);

        // Add label
        $html .= $this->label($item['options']['id'], $item['placeholder']);

        return $html;
    }

    /**
     * Container for checkbox / radio group
     *
     * @param $type
     * @param $inputArray
     * @param $checked
     * @param $errors
     * @param array $options
     * @return string
     * @throws \Psr\Log\InvalidArgumentException
     */
    private function multiGroup($type, $inputArray, $checked, $errors, $options = []){
        $html = '';

        if(!isset($inputArray['name']))
            throw new InvalidArgumentException($type.' name attribute not defined.');

        if(!isset($inputArray[$type]))
            throw new InvalidArgumentException('No '.$type.' input fields defined.');

        if ($type == "radios") {
            $options['wrapperClass'] = isset($options['wrapperClass']) ? $options['wrapperClass'] . ' form-group form-group-radio ' : 'form-group form-group-radio ';
        } elseif ($type == "checkboxes") {
            $options['wrapperClass'] = isset($options['wrapperClass']) ? $options['wrapperClass'] . ' form-group form-group-checkbox ' : 'form-group form-group-checkbox ';
        }

        // Format checked array
        if(is_multi_array($checked)) {
            if(!isset(array_values($checked)[0]['id'])) {
                $checked = (array_flatten($checked));
            } else {
                $checked = array_pluck($checked, 'id');
            }
        }

        // Loop through input fields heen
        foreach($inputArray[$type] as $key => $input) {

            // Check if we need to add checkboxinput or radio input
            if($type == 'checkboxes')
                $html .= $this->checkboxInput($input, $inputArray['name'].'['.$key.']', $this->getChecked($input['value'], $checked));

            elseif($type == 'radios')
                $html .= $this->radioInput($input, $inputArray['name'], $this->getChecked($input['value'], $checked));

            // Add error class when error found
            if($errors and $errors->has($inputArray['name'])) {
                $options['wrapperClass'] = isset($options['wrapperClass']) ? $options['wrapperClass'] . ' form-error ' : 'form-error ';
            }
        }

        return $this->wrap($html, $options);
    }


    /**
     * Format given array for selectField helper
     *
     * @param $inputItems
     * @return array
     */
    public function formatMultiInputs($inputItems)
    {
        $formattedArray = array();

        foreach($inputItems as $key => $input) {
            $formattedArray[$key] = [
               'value'          => $key,
               'placeholder'    => $input,
            ];
        }

        return $formattedArray;
    }

    /**
     * Check if value is checked based on checkedValues array
     *
     * @param $key
     * @param $checkedValues
     * @return bool
     */
    public function getChecked($key, $checkedValues)
    {
        if(!is_array($checkedValues)){
            if($key == $checkedValues)
                return true;

            return false;
        }

        elseif(in_array($key, $checkedValues))
            return true;

        return false;
    }


    /**
     * @param $name
     * @param $values
     * @param bool $selected
     * @param array $options
     * @param array $errors
     * @return string
     */
    public function selectField($name, $values, $selected = false, $options = [], $errors = [])
    {
        $html = '';

        // Disabled standaard - placeholder: Optie 1 bijv. selecteer een optie. Wanneer false geen placeholder. Wanneer true gewoon leeg -> default
        $default = ['class'   => 'show-dropdown', 'id' => $name, 'placeholder' => true];
        $options = array_merge($default, $options);

        // Check if placeholder is set
        if((($selected === false || is_null($selected) || $selected == 0) && $options['placeholder'] !== false)
            || (isset($options['placeholder']) && $options['placeholder'] === true) || !isset($options['placeholder'])) {

            $values = ['placeholderValue' => (is_bool($options['placeholder']) && $options['placeholder'] == true) ? '' : $options['placeholder']] + $values;
            $selected = 'placeholderValue';
        }

        // Check if there are any errors 
        if(isset($errors) && is_object($errors) && $errors->has($name)) {
            $options['wrapperClass'] = (isset($options['wrapperClass'])) ? $options['wrapperClass'] . ' form-error ' : $this->defaultWrapperClass . ' form-error ';
        }

        // Check if tooltip set, if so add tooltip components
        if(isset($options['tooltip']))
            $html .= '<div class="info-tooltip"><span href="#" class="icon-info show-tooltip" title="'.$options['tooltip'].'"></span></div>';

        $html .= $this->select($name, $values, $selected, array_except($options, ['label', 'before', 'after', 'wrapperClass', 'tooltip', 'placeholder']));

        return $this->wrap($html, $options);
    }

    /**
     * @param $name
     * @param $values
     * @param bool $selected
     * @param array $options
     * @param array $errors
     * @return mixed
     */
    public function multiSelectField($name, $values, $selected = false, $options = [], $errors = [])
    {
        return $this->chosenSelectField('multi', $name, $values, $selected, $options, $errors);
    }

    /**
     * @param $name
     * @param $values
     * @param bool $selected
     * @param array $options
     * @param array $errors
     * @return mixed
     */
    public function singleSelectField($name, $values, $selected = false, $options = [], $errors = [])
    {
        return $this->chosenSelectField('single', $name, $values, $selected, $options, $errors);
    }

    /**
     * Add chosen selectfield
     *
     * @param       $type
     * @param       $name
     * @param       $values
     * @param bool  $selected
     * @param array $options
     * @param array $errors
     *
     * @return string
     */
    private function chosenSelectField($type, $name, $values, $selected = false, $options = [], $errors = []){

        // Set default options
        if($type == 'single') {
            $default = ['class' => 'show-chosen', 'single'];
        } else {
            $default = ['class' => 'show-chosen', 'multiple'];
        }

        // Check if placeholder is set
        if(isset($options['placeholder'])) {

            // Set the placeholder for this multiSelectField
            $options['data-placeholder'] = $options['placeholder'];
        } else {

            // Default placeholder
            $options['data-placeholder'] = 'Selecteer een optie';
        }

        // Set placeholder to false
        $options['placeholder'] = false;

        // Merge with given options
        $options = array_merge($default, $options);

        // Return selectField
        return $this->selectField($name,$values,$selected,$options,$errors);
    }


    /**
     * Extend the default select element option.
     *
     * @param  string  $display
     * @param  string  $value
     * @param  string  $selected
     * @return string
     */
    protected function option($display, $value, $selected)
    {
        $selected = $this->getSelectedValue($value, $selected);
        $options = array('value' => e($value), 'selected' => $selected);

        // Check if the value is equal to placeholderValue, if so we're dealing with a placeholder
        // reset value and set it to disabled
        if($value === 'placeholderValue') {
            $options['disabled'] = '';
            $options['value']   = '';
        }

        return '<option'.$this->html->attributes($options).'>'.e($display).'</option>';
    }

    /**
     * @param $name
     * @param $placeholder
     * @param $errors
     * @param array $options
     * @param null $value
     * @param $checked
     * @return string
     */
    public function checkboxField($name, $placeholder, $errors, $options = [], $value = null, $checked = false)
    {
        $default = [
            'id'    => $name,
        ];

        $options = array_merge($default, $options);

        if($checked === true) {
            $options['checked'] = true;
        }

        $html = '';

        // Always post checkbox field, also when it's false (http://nielson.io/2014/02/handling-checkbox-input-in-laravel/)
        if($value == 1)
            $html .= $this->hidden($name, false, array('id' => $name . '_hidden'));

        $html .= $this->checkbox($name, $value, $checked, $options);
        $html .= $this->label($options['id'], $placeholder);
        
        // Add Tooltip if provided
        if(isset($options['tooltip']))
            $html .= '<div class="info-tooltip"><span href="#" class="icon-info show-tooltip" title="'.$options['tooltip'].'"></span></div>';              

        if($errors and $errors->has($name)) {
            $options['wrapperClass'] = isset($options['wrapperClass']) ? $options['wrapperClass'] . ' form-error ' :  $this->defaultWrapperClass . ' form-error ';
        }

        return $this->wrap($html, $options);
    }

    /**
     * @param $name
     * @param $placeholder
     * @param $errors
     * @param array $options
     * @param null $value
     * @return string
     */
    public function datePicker($name, $placeholder, $errors, $options = [], $value = null)
    {
        $html = '';

        // Set datepicker config
        $options['class'] = isset($options['class']) ? $options['class'] . ' show-datepicker ' : 'show-datepicker ';
        $options['id'] = isset($options['id']) ? $options['id'] : str_replace(' ', '_', lcfirst($name));
        $options['data-date-format'] = isset($options['data-date-format']) ? $options['data-date-format'] : 'dd-mm-yyyy';

        // Allow the user to set data before html input
        if(isset($options['before']))
            $html .= $options['before'];

        // Add error class when error found
        if($errors and $errors->has($name)) {
            $options['class'] = isset($options['class']) ? $options['class'] . ' form-error ' : 'form-error ';
        }

        // Create label
        $html .= $this->label( $options['id'], $placeholder);

        // Check if label is set. If so - just show normal label by not adding the class has-label-placeholder
        if(!isset($options['label']) or $options['label'] == false) {
            $options['wrapperClass'] = isset($options['wrapperClass']) ? $options['wrapperClass'] . ' has-label-placeholder' : $this->defaultWrapperClass. ' has-label-placeholder ';
        }

        // Add placeholder
        $html .= $this->input('text', $name, $value, array_except($options, ['label', 'before', 'after', 'wrapperClass', 'tooltip', 'placeholder'])) . '<label class="form-icon icon-calendar4" for="'.$options['id'].'"></label>';

        // Allow the user to set data before html input
        if(isset($options['after']))
            $html .= $options['after'];

        return $this->wrap($html, $options);
    }

    /**
     * Radio button group gender helper
     *
     * @param $name
     * @param $value
     * @param $errors
     * @param array $options
     * @return string
     */
    public function radioGender($name, $value, $errors, $options = [] )
    {
        // Add error class when error found
        if($errors and $errors->has($name)) {
            $options['wrapperClass'] = isset($options['wrapperClass']) ? $options['wrapperClass'] . ' form-error ' : 'form-error ';
        }

        $options['wrapperClass'] = isset($options['wrapperClass']) ? $options['wrapperClass'] . ' form-group form-center form-group-radio ' : 'form-group form-center form-group-radio ';

        $html = $this->radio($name, 'Man', (($value == 'man') ? true : false), ['id' => 'man']);
        $html .= $this->label('man', 'Man');

        $html .= $this->radio($name, 'Vrouw',  (($value == 'vrouw') ? true : false), ['id' => 'vrouw']);
        $html .= $this->label('vrouw', 'Vrouw');

        return $this->wrap($html, $options);
    }

    /**
     * Field function, used by textField, passwordField etc.
     *
     * @param $type
     * @param $name
     * @param $placeholder
     * @param $errors
     * @param array $options
     * @param null $value
     * @return string
     */
    private function field($type, $name, $placeholder, $errors, $options = [], $value = null)
    {
        $html = '';

        // Add error when found
        if($errors and $errors->has($name)) {
            $options['class'] = isset($options['class']) ? $options['class'] . ' form-error ' : 'form-error ';
        }

        // Allow the user to set data before html input
        if(isset($options['before']))
            $html .= $options['before'];

        // Create label
        if ($type == "time"){
            $html .= $this->label($name, $placeholder, ['class' => 'time-field']);
        } elseif( !(isset($options['label'])) || (isset($options['label']) && $options['label'] == true) ) {
            $html .= $this->label($name, $placeholder);
        }

        $options['maxlength'] = Config::get('formbuilder::maxlength');

        // Check if label is set. If so - just show normal label by not adding the class has-label-placeholder
        if( ! isset($options['label']) ) {
            $options['wrapperClass'] = isset($options['wrapperClass']) ? $options['wrapperClass'] . ' has-label-placeholder' : $this->defaultWrapperClass. ' has-label-placeholder ';
        }

        // Add options to input, except options that should not be used for this input
        $html .= $this->input($type, $name, $value,  array_except($options, ['label', 'before', 'after', 'wrapperClass', 'tooltip', 'placeholder']));

        // Allow the user to set after data
        if(isset($options['after']))
            $html .= $options['after'];

        // Check if tooltip set, if so add tooltip components
        if(isset($options['tooltip']))
            $html .= '<div class="info-tooltip"><span href="#" class="icon-info show-tooltip" title="'.$options['tooltip'].'"></span></div>';

        return $this->wrap($html, $options);
    }

    /**
     * @param $html
     * @param $options
     * @return string
     */
    private function wrap($html, $options)
    {
        // Override wrapperClass, or add default
        $wrapperClass = isset($options['wrapperClass']) ? $options['wrapperClass'] : $this->defaultWrapperClass;
        return "<div class='$wrapperClass'>$html</div>";
    }
    
    /**
    *    Country select
    *
    */
    public function countrySelect($name, $placeholder, $errors = array(), $selected = false) {
        $html = $this->label($name, $placeholder, ['class' => 'label-left']);
        $html .= $this->selectField($name, array('Nederland' => 'Nederland', 'België' => 'België'), Input::old($name, $selected), ['placeholder' => 'Land', 'wrapperClass' => 'form-group form-large float-right'], $errors);
        
        return $html;
    }
}